package migrations

import (
	"bitbucket.org/botsolve/qor/activity"
	"bitbucket.org/botsolve/qor/auth/auth_identity"
	"bitbucket.org/botsolve/qor/banner_editor"
	"bitbucket.org/botsolve/qor/help"
	"bitbucket.org/botsolve/qor/media/asset_manager"
	"bitbucket.org/botsolve/qor/qor-example/app/models"
	"bitbucket.org/botsolve/qor/qor-example/config/admin"
	"bitbucket.org/botsolve/qor/qor-example/config/seo"
	"bitbucket.org/botsolve/qor/qor-example/db"
	"bitbucket.org/botsolve/qor/transition"
)

func init() {
	AutoMigrate(&asset_manager.AssetManager{})

	AutoMigrate(&models.Product{}, &models.ProductVariation{}, &models.ProductImage{}, &models.ColorVariation{}, &models.ColorVariationImage{}, &models.SizeVariation{})
	AutoMigrate(&models.Color{}, &models.Size{}, &models.Material{}, &models.Category{}, &models.Collection{})

	AutoMigrate(&models.Address{})

	AutoMigrate(&models.Order{}, &models.OrderItem{})

	AutoMigrate(&models.Store{})

	AutoMigrate(&models.Setting{})

	AutoMigrate(&models.User{})

	AutoMigrate(&transition.StateChangeLog{})

	AutoMigrate(&activity.QorActivity{})

	AutoMigrate(&admin.QorWidgetSetting{})

	AutoMigrate(&models.Page{})

	AutoMigrate(&seo.MySEOSetting{})

	AutoMigrate(&models.MediaLibrary{})

	AutoMigrate(&models.Article{})

	AutoMigrate(&help.QorHelpEntry{})

	AutoMigrate(&auth_identity.AuthIdentity{})

	AutoMigrate(&banner_editor.QorBannerEditorSetting{})
}

func AutoMigrate(values ...interface{}) {
	for _, value := range values {
		db.DB.AutoMigrate(value)
	}
}
