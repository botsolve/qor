// +build enterprise

package migrations

import (
	"bitbucket.org/botsolve/qor/qor-example/config/admin"
)

func init() {
	AutoMigrate(&admin.QorMicroSite{})
}
