package models

import (
	"strings"

	"github.com/jinzhu/gorm"
	"bitbucket.org/botsolve/qor/l10n"
	"bitbucket.org/botsolve/qor/sorting"
	"bitbucket.org/botsolve/qor/validations"
)

type Color struct {
	gorm.Model
	l10n.Locale
	sorting.Sorting
	Name string
	Code string `l10n:"sync"`
}

func (color Color) Validate(db *gorm.DB) {
	if strings.TrimSpace(color.Name) == "" {
		db.AddError(validations.NewError(color, "Name", "Name can not be empty"))
	}

	if strings.TrimSpace(color.Code) == "" {
		db.AddError(validations.NewError(color, "Code", "Code can not be empty"))
	}
}
