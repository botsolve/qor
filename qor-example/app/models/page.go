package models

import (
	"bitbucket.org/botsolve/qor/page_builder"
	"bitbucket.org/botsolve/qor/publish2"
)

type Page struct {
	page_builder.Page

	publish2.Version
	publish2.Schedule
	publish2.Visible
}
