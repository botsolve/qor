package models

import (
	"github.com/jinzhu/gorm"
	"bitbucket.org/botsolve/qor/l10n"
)

type Collection struct {
	gorm.Model
	Name string
	l10n.LocaleCreatable
}
