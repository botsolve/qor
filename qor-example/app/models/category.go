package models

import (
	"fmt"
	"strings"

	"github.com/jinzhu/gorm"
	"bitbucket.org/botsolve/qor/l10n"
	"bitbucket.org/botsolve/qor/sorting"
	"bitbucket.org/botsolve/qor/validations"
)

type Category struct {
	gorm.Model
	l10n.Locale
	sorting.Sorting
	Name string
	Code string

	Categories []Category
	CategoryID uint
}

func (category Category) Validate(db *gorm.DB) {
	if strings.TrimSpace(category.Name) == "" {
		db.AddError(validations.NewError(category, "Name", "Name can not be empty"))
	}
}

func (category Category) DefaultPath() string {
	if len(category.Code) > 0 {
		return fmt.Sprintf("/category/%s", category.Code)
	}
	return "/"
}
