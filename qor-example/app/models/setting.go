package models

import (
	"github.com/jinzhu/gorm"
	"bitbucket.org/botsolve/qor/location"
	"bitbucket.org/botsolve/qor/l10n"
)

type FeeSetting struct {
	ShippingFee     uint
	GiftWrappingFee uint
	CODFee          uint `gorm:"column:cod_fee"`
	TaxRate         int
}

type Setting struct {
	gorm.Model
	FeeSetting
	location.Location `location:"name:Company Address"`
	l10n.Locale
}
