package controllers

import (
	"net/http"

	"bitbucket.org/botsolve/qor/qor"
	"bitbucket.org/botsolve/qor/qor-example/config"
	"bitbucket.org/botsolve/qor/qor/utils"
)

func HomeIndex(w http.ResponseWriter, req *http.Request) {
	config.View.Execute("home_index", map[string]interface{}{}, req, w)
}

func SwitchLocale(w http.ResponseWriter, req *http.Request) {
	utils.SetCookie(http.Cookie{Name: "locale", Value: req.URL.Query().Get("locale")}, &qor.Context{Request: req, Writer: w})
	http.Redirect(w, req, req.Referer(), http.StatusSeeOther)
}
