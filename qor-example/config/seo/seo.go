package seo

import (
	"bitbucket.org/botsolve/qor/l10n"
	"bitbucket.org/botsolve/qor/seo"
)

type MySEOSetting struct {
	seo.QorSEOSetting
	l10n.Locale
}

type SEOGlobalSetting struct {
	SiteName string
}

var SEOCollection *seo.Collection
