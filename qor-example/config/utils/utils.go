package utils

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/jinzhu/gorm"
	"bitbucket.org/botsolve/qor/l10n"
	"bitbucket.org/botsolve/qor/qor-example/app/models"
	"bitbucket.org/botsolve/qor/qor-example/config/admin"
	"bitbucket.org/botsolve/qor/qor-example/config/auth"
	"bitbucket.org/botsolve/qor/qor-example/db"
	"bitbucket.org/botsolve/qor/qor/utils"
)

// GetCurrentUser get current user from request
func GetCurrentUser(req *http.Request) *models.User {
	if currentUser, ok := auth.Auth.GetCurrentUser(req).(*models.User); ok {
		return currentUser
	}
	return nil
}

// GetCurrentLocale get current locale from request
func GetCurrentLocale(req *http.Request) string {
	locale := l10n.Global
	if cookie, err := req.Cookie("locale"); err == nil {
		locale = cookie.Value
	}
	return locale
}

// GetEditMode get edit mode
func GetEditMode(w http.ResponseWriter, req *http.Request) bool {
	return admin.ActionBar.EditMode(w, req)
}

// GetDB get DB from request
func GetDB(req *http.Request) *gorm.DB {
	if db := utils.GetDBFromRequest(req); db != nil {
		return db
	}
	return db.DB
}

// URLParam get url params from request
func URLParam(name string, req *http.Request) string {
	return chi.URLParam(req, name)
}
