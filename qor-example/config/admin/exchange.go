package admin

import (
	"bitbucket.org/botsolve/qor/exchange"
	"bitbucket.org/botsolve/qor/qor"
	"bitbucket.org/botsolve/qor/qor/resource"
	"bitbucket.org/botsolve/qor/qor/utils"
	"bitbucket.org/botsolve/qor/validations"

	"bitbucket.org/botsolve/qor/qor-example/app/models"
)

var ProductExchange = exchange.NewResource(&models.Product{}, exchange.Config{PrimaryField: "Code"})

func init() {
	ProductExchange.Meta(&exchange.Meta{Name: "Code"})
	ProductExchange.Meta(&exchange.Meta{Name: "Name"})
	ProductExchange.Meta(&exchange.Meta{Name: "Price"})

	ProductExchange.AddValidator(func(record interface{}, metaValues *resource.MetaValues, context *qor.Context) error {
		if utils.ToInt(metaValues.Get("Price").Value) < 100 {
			return validations.NewError(record, "Price", "price can't less than 100")
		}
		return nil
	})
}
