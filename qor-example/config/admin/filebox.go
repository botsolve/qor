package admin

import (
	"bitbucket.org/botsolve/qor/filebox"
	"bitbucket.org/botsolve/qor/roles"

	"bitbucket.org/botsolve/qor/qor-example/config"
	"bitbucket.org/botsolve/qor/qor-example/config/auth"
)

var Filebox *filebox.Filebox

func init() {
	Filebox = filebox.New(config.Root + "/public/downloads")
	Filebox.SetAuth(auth.AdminAuth{})
	dir := Filebox.AccessDir("/")
	dir.SetPermission(roles.Allow(roles.Read, "admin"))
}
