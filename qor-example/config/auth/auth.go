package auth

import (
	"bitbucket.org/botsolve/qor/auth"
	"bitbucket.org/botsolve/qor/auth/authority"
	"bitbucket.org/botsolve/qor/auth_themes/clean"
	"bitbucket.org/botsolve/qor/qor-example/app/models"
	"bitbucket.org/botsolve/qor/qor-example/config"
	"bitbucket.org/botsolve/qor/qor-example/db"
)

var (
	// Auth initialize Auth for Authentication
	Auth = clean.New(&auth.Config{
		DB:         db.DB,
		Render:     config.View,
		Mailer:     config.Mailer,
		UserModel:  models.User{},
		Redirector: auth.Redirector{RedirectBack: config.RedirectBack},
	})

	// Authority initialize Authority for Authorization
	Authority = authority.New(&authority.Config{
		Auth: Auth,
		RedirectPathAfterAccessDenied: "/auth/login",
	})
)

// var Auth = auth.New(&auth.Config{
// 	DB:        db.DB,
// 	Render:    config.View,
// 	Mailer:    config.Mailer,
// 	UserModel: models.User{},
// })

// func init() {
// 	Auth.RegisterProvider(password.New(&password.Config{Confirmable: true}))
// 	Auth.RegisterProvider(phone.New())
// 	Auth.RegisterProvider(github.New(&config.Config.Github))
// 	Auth.RegisterProvider(google.New(&config.Config.Google))
// }
