// +build enterprise

package routes

import "bitbucket.org/botsolve/qor/qor-example/config/admin"

func init() {
	Router()
	WildcardRouter.AddHandler(admin.MicroSite)
}
