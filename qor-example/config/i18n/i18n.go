package i18n

import (
	"path/filepath"

	"bitbucket.org/botsolve/qor/i18n"
	"bitbucket.org/botsolve/qor/i18n/backends/database"
	"bitbucket.org/botsolve/qor/i18n/backends/yaml"

	"bitbucket.org/botsolve/qor/qor-example/config"
	"bitbucket.org/botsolve/qor/qor-example/db"
)

var I18n *i18n.I18n

func init() {
	I18n = i18n.New(database.New(db.DB), yaml.New(filepath.Join(config.Root, "config/locales")))
}
