package manager

import (
	"github.com/gorilla/sessions"
	"bitbucket.org/botsolve/qor/session"
	"bitbucket.org/botsolve/qor/session/gorilla"
)

// SessionManager default session manager
var SessionManager session.ManagerInterface = gorilla.New("_session", sessions.NewCookieStore([]byte("secret")))
