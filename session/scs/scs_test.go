package scs_test

import (
	"testing"

	"github.com/alexedwards/scs/engine/memstore"
	"bitbucket.org/botsolve/qor/session/scs"
	"bitbucket.org/botsolve/qor/session/test"
)

func TestAll(t *testing.T) {
	engine := memstore.New(0)
	manager := scs.New(engine)
	test.TestAll(manager, t)
}
