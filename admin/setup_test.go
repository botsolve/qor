package admin_test

import (
	"net/http/httptest"

	"github.com/jinzhu/gorm"
	"bitbucket.org/botsolve/qor/admin"
	. "bitbucket.org/botsolve/qor/admin/tests/dummy"
)

var (
	server *httptest.Server
	db     *gorm.DB
	Admin  *admin.Admin
)

func init() {
	Admin = NewDummyAdmin()
	db = Admin.Config.DB
	server = httptest.NewServer(Admin.NewServeMux("/admin"))
}
