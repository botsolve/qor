# QOR Help

QOR Help provides a way to add help documents to [QOR Admin](http://github.com/qor/admin)

## Documentation

<https://doc.getqor.com/plugins/help.html>

## License

Released under the [MIT License](http://opensource.org/licenses/MIT).
