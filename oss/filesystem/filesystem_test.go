package filesystem

import (
	"testing"

	"bitbucket.org/botsolve/qor/oss/tests"
)

func TestAll(t *testing.T) {
	fileSystem := New("/tmp")
	tests.TestAll(fileSystem, t)
}
